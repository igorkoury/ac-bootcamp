var jediName; // your Jedi name
var forcePower = 0; // force power gained meditating at the Jedi Temple
var bountyValue = 0; // starts at 0, if it reaches 100 you get busted and lose the game
var fear = 0; // starts at 0, if it reaches 100 you join the Dark side and lose the game
var vulnerabilities = 0; // number of vulnerabilities found
var jediKnight = false; // boolean to indicate if the player has reached Jedi knight level
var hitpoints = 30 // your current hitpoints
var ancientTexts = [
  { chapter: 1, technique: '□Fo□□rc□e L□ea□p□□' },
  { chapter: 2, technique: 'T□ra□□n□□□s□fe□r F□o□rc□e□□' },
  { chapter: 3, technique: 'Fo□□rc□e B□urs□t' },
  { chapter: 4, technique: '□□M□alac□i□a' },
  { chapter: 5, technique: '□Fo□rce□□ We□a□□po□n' }
];
var sithLord = {
  name: 'Darth Tekman',
  hitpoints: 45,
  counterImmune: false
};

function showStats() {
    console.log(
      '===========================' +
      '\n       CURRENT STATS' +
      '\n===========================' +
      '\n» Force Power: ' + forcePower +
      '\n» Bounty: ' + bountyValue +
      '\n» Fear: ' + fear +
      '\n» Vunerabilities: ' + vulnerabilities +
      '==========================='
    );
  };

  showStats();
